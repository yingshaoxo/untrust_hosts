# untrust_hosts

## How to use this repo?
Copy following code, paste it into `/etc/hosts`:

```
0.0.0.0 github.com
0.0.0.0 microsoft.com
```

## untrust_reasons
github.com: After microsoft bought github, they banned my account for no clear reason, I have used it for almost 10 years.